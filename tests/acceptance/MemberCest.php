<?php namespace App\Tests;
use App\Tests\AcceptanceTester;

class MemberCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function tryToLogin(AcceptanceTester $I)
    {
        $I->amOnPage('/login');

        $I->see('Log in');
    }

    public function tryToRegister(AcceptanceTester $I)
    {
        $I->amOnPage('/register');

        $I->see('Register');
    }
}
