<?php namespace App\Tests;

class LoginTest extends \Codeception\Test\Unit
{
    /**
     * @var \App\Tests\FunctionalTester
     */
    protected $tester;
    
    protected function _before()
    {
    }

    protected function _after()
    {
    }

    public function testLogin()
    {
        $this->tester->amOnPage('/login');

        $this->tester->see('Log in');
    }
}