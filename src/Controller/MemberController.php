<?php

namespace App\Controller;

use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MemberController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/member", name="app_members")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $members = $this->entityManager->getRepository(Member::class)->findAll();
        $friends = $this->getUser()->getFriends();
        $other_members = array_filter($members, function($member) use ($friends) {
            return !$friends->contains($member);
        });

        return $this->render('member/index.html.twig', [
            'controller_name' => 'MemberController',
            'friends' => $friends,
            'members' => $other_members
        ]);
    }

    /**
     * @Route("/member/add_friend", name="app_add_friend")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFriend(Request $request)
    {
        $user = $this->getUser();
        $friend = $this->entityManager->getRepository(Member::class)->findOneBy(['user' => $request->query->get('friend')]);

        if ($friend != $user)
            $user->addFriend($friend);

        $this->entityManager->persist($user);
        $this->entityManager->flush();


        return $this->index();
    }

    /**
     * @Route("/member/remove_friend", name="app_remove_friend")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function removeFriend(Request $request)
    {
        $user = $this->getUser();
        $friend = $this->entityManager->getRepository(Member::class)->findOneBy(['user' => $request->query->get('friend')]);

        $user->removeFriend($friend);

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $this->index();
    }
}
