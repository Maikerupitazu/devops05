<?php

namespace App\Controller;

use App\Entity\Member;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LivesearchController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/livesearch", name="app_live_search")
     */
    public function index(Request $request)
    {
        $response = '';

        $rsm = new ResultSetMappingBuilder($this->entityManager);
        $rsm->addRootEntityFromClassMetadata('App:Member', 'm');
        $query = $this->entityManager->createNativeQuery('SELECT * FROM member m WHERE user LIKE "%' . $request->get('q') . '%" ORDER BY user ASC', $rsm);
        $members = $query->getResult();

        foreach ($members as $member) {
            $response .= '<a href="profile?user=' . $member->getUser() . '">' . $member->getUser() . '</a><br>';
        }

        return new Response(
            $response,
            Response::HTTP_OK,
            ['content-type' => 'text/html']
        );
    }
}
