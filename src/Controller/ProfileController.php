<?php

namespace App\Controller;

use App\Entity\Member;
use App\Entity\Picture;
use App\Form\Type\PictureType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/profile", name="app_profile")
     */
    public function index(Request $request)
    {
        $member = $this->entityManager->getRepository(Member::class)
            ->findOneBy(['user' => $request->query->get('user')]);

        if (!$member)
            throw $this->createNotFoundException('The member does not exist');

        return $this->render('profile/index.html.twig', [
            'controller_name' => 'ProfileController',
            'member' => $member
        ]);
    }

    /**
     * @Route("/profile/edit", name="app_profile_edit", methods={"POST"})
     */
    public function edit(Request $request)
    {
        $member = $this->getUser();
        $profile = $request->get('profile');
        $picture = $request->get('picture');

        $member->setPicture($picture);
        $member->setProfile($profile);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($member);
        $entityManager->flush();

        return $this->redirectToRoute('app_profile', ['user' => $member->getUser()]);
    }
}
